(function ($, Drupal) {

  Drupal.behaviors.heroSlider = {
    attach: function (context) {
      // Append play/pause button.
      $(".view-hero-slider .view-content", context).on('init', function (event, slick) {
        if (slick.$dots) {
          slick.$dots.append('<li><button class="play"></button></li>');
        }
      });

      // Initialize hero slider.
      $(".view-hero-slider .view-content", context).slick({
        infinite: true,
        dots: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              autoplay: false,
              centerMode: true,
              slidesToShow: 1
            }
          },
        ]
      });

      // Add play/pause behavior.
      $('.play', context).click(function () {
        if ($(this).hasClass('paused')) {
          $(this).removeClass('paused');
          $('.view-hero-slider .view-content').slick('slickPlay');
          return;
        }
        $(this).addClass('paused');
        $('.view-hero-slider .view-content').slick('slickPause');
      });

      // Mobile view.
      $(".node--type-slide", context).each(function (key, value) {
        let $element = $(value).find('.field--type-link a');
        if ($element.is(':hidden')) {
          $(value).find('.node__content-inner').css('cursor', 'pointer').click(function () {
            window.location = $element.attr('href');
          });
        }
      });
    }
  };

})(jQuery, Drupal);
